#!/usr/bin/env bash
echo "Deploy script started"
source ~/CICD/myenv/bin/activate
cd /home/omorboshir/CICD/cicd-server
git pull
echo "Deploy script finished execution"
